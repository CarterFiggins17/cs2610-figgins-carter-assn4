// Re-set the document title
document.title = 'Fig\'s FibTree';

// Create a red div in the body of the document
var div = document.createElement('div');
div.setAttribute('class', 'green fib-container');
document.querySelector('body').appendChild(div);

// Make a paragraph to hold some instructions
var para = document.createElement('p');
para.textContent = "Fibonacci Tree";
div.appendChild(para);

var theForm = document.createElement('form');
div.appendChild(theForm);

var theLable = document.createElement('lable');
theLable.setAttribute('for', 'tree-slider');
theLable.setAttribute('id','tree-lable');
theLable.textContent = "Fibonacci Tree:(0)"
theForm.appendChild(theLable);

var slider = document.createElement('input');
slider.setAttribute('id', 'tree-slider')
slider.setAttribute('type', 'range')
slider.setAttribute('min', '0')
slider.setAttribute('max', '11')
slider.setAttribute('value', '0')
slider.setAttribute('oninput', 'treeSlider(this)')
theForm.appendChild(slider);

var divTree = document.createElement('div');
divTree.setAttribute('id', 'tree-of-divs');
divTree.setAttribute('class', 'fib-container');
divTree.setAttribute('class', 'blue');
div.appendChild(divTree);


var recursiveBinTree = function(depth){
	var newDiv = document.createElement('div');
	newDiv.setAttribute('class', 'fib-item');
	var newP = document.createElement('p');
	
	newDiv.appendChild(newP)
	
	if(depth === 0){
		newP.textContent = `Fib(${depth}) = 0`;
		var myObj = {
				div: newDiv,
				value: 0,		
			}
		return myObj;
	}
	if(depth === 1){
		newP.textContent = `Fib(${depth}) = 1`;

		return {div: newDiv,
				value: 1,}
	}
	else{

		
		var left = recursiveBinTree(depth - 1);
		var cls = left.div.getAttribute('class');
		left.div.setAttribute('class', `fib-left ${cls}`);
		newDiv.appendChild(left.div);
		
		var right = recursiveBinTree(depth - 2);
		cls = right.div.getAttribute('class');
		right.div.setAttribute('class', `fib_right ${cls}`);
		newDiv.appendChild(right.div);
		
		var fibNum = (left.value) + (right.value);
		newP.textContent = `Fib(${depth}) = ${fibNum}`
		return {div: newDiv,
				value: fibNum};
		
	}
	
}


var treeSlider = function(slider){
	var form1 = slider.parentNode;
	
	var value = parseInt(slider.value);
	console.log(value);
	
	var lable = document.querySelector('#tree-lable');
	lable.textContent = `Fibonacci Tree: (${value})`;
	
	var tree = document.querySelector('#tree-of-divs');
	if(tree){
		tree.remove();
	}
	
	
	
	tree = document.createElement('div');
	tree.setAttribute ('id', 'tree-of-divs');
	tree.setAttribute('class', 'fib-container')
	
	var treeObj = recursiveBinTree(value);
	tree.appendChild(treeObj.div);

	
	form1.parentNode.appendChild(tree);
	
	
}









